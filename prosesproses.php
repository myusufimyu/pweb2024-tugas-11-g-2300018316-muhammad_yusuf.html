<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST['sepatu'])){
    $sepatuPilihan = $_POST['sepatu'];

    echo "<h2>Vote Sepatu Favorit Anda Adalah:</h2>";
    echo "<ul>";

    foreach ($sepatuPilihan as $sepatu) {
      echo "<li>" . htmlspecialchars($sepatu) . "</li>";
    }

    echo "</ul>";

  }else{
    echo "Anda belum memilih brand sepatu.";
  }

}
?>
