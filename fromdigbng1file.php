<!DOCTYPE html>
<html>
<head>
    <title>Pengolahan Form cara #1</title>
    <style>
        body {
            font-family: sans-serif;
        }
        

        form {
            width: 300px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        .logosaya {
         display: block;
         margin-left: auto;
         margin-right: auto;
         width: 60px;
         padding : 0px;
         border-radius: 32px;
         box-shadow: 0 0 5px #770000;
  }

        input[type="text"] {
            width: 95%;
            padding: 10px;
            margin: 5px 0;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #26a69a;
            color: white;
            padding: 10px 15px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }
        

label {

    </style>
</head>
<body>
<img src="logosaya.png" alt="paris" class="logosaya"><br>

    <form action="" method="post">
        
        <label for="nama">Nama Anda:</label><br>
        <input type="text" id="nama" name="nama"><br><br>
        <input type="submit" name="Input" value="Tampilkan Nama">
    </form>

    <?php
    if (isset($_POST['Input'])) {
        $nama = $_POST['nama'];
        if (!empty($nama)) {  
            echo "<p>Halo, <b>$nama</b>! Selamat datang.</p>";
        } else {
            echo "<p>Silakan masukkan nama Anda.</p>";
        }
    }
    ?>

</body>
</html>